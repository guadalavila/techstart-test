# TEST TECHSTART

This is the backend for test techstart

# Getting started

- Clone the repository

```
git clone https://gitlab.com/Chasty1/techstart-test.git
```

### Populate data

```
Use the db directory and upload the json contents into MongoDB using Studio3T import
```

# Installation

- Install dependencies

```
cd techstart-test
npm install
npm run build
```

- Launch demo Node and Mongo server in docker containers

```
docker-compose build
docker-compose up
```

( _Alternatively, you can run and configure your local or cloud Mongo server and start Node server with
`npm run build && npm start`_)

Please check package.json for other useful npm scripts (for example typescript and nodemon watchers in development)
