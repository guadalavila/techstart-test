import { Router } from "express";
import { ManufacturerController } from "../controllers/manufacturerController";

export class ManufacturerRoutes {
  public router: Router;
  public manufacturerController: ManufacturerController = new ManufacturerController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", this.manufacturerController.getManufacturer);
    this.router.post("/create", this.manufacturerController.createManufacturer);
  }
}
