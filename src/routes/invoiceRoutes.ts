import { Router } from "express";
import { InvoiceController } from "../controllers/invoiceController";

export class InvoiceRoutes {
  public router: Router;
  public invoiceController: InvoiceController = new InvoiceController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", this.invoiceController.getInvoices);
    this.router.post("/create", this.invoiceController.createInvoice);
    this.router.post(
      "/getByProduct",
      this.invoiceController.getInvoicesByProduct
    );
    this.router.post(
      "/getByDistributor",
      this.invoiceController.getInvoicesByDistributor
    );
    this.router.post(
      "/getByCustomer",
      this.invoiceController.getInvoicesByCustomerLocation
    );
    this.router.post(
      "/getByInvoice",
      this.invoiceController.getInvoicesByInvoice
    );
    this.router.get(
      "/getDistinticInvoices",
      this.invoiceController.getDifferentInvoices
    );
  }
}
