import { Router } from "express";
import { ProductoController } from "../controllers/productoController";

export class ProductoRoutes {
  public router: Router;
  public productoController: ProductoController = new ProductoController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", this.productoController.getProductos);
    this.router.post("/create", this.productoController.create);
  }
}
