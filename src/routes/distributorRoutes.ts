import { Router } from "express";
import { DistributorController } from "../controllers/distributorController";

export class DistributorRoutes {
  public router: Router;
  public distributorRoutes: DistributorController = new DistributorController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", this.distributorRoutes.getDistributors);
    this.router.post("/create", this.distributorRoutes.create);
  }
}
