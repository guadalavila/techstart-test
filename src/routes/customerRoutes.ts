import { Router } from "express";
import { CustomerController } from "../controllers/customerController";

export class CustomerRoutes {
  public router: Router;
  public customerController: CustomerController = new CustomerController();

  constructor() {
    this.router = Router();
    this.routes();
  }

  routes() {
    this.router.get("/", this.customerController.getCustomers);
    this.router.post("/create", this.customerController.create);
  }
}
