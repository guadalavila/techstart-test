import { Request, Response } from "express";
import service from "../../services/getRepresentantes/service";

export class RepresentantesController {

    public async getRepresentantes(req: Request, res: Response): Promise<void> {
        const representantes = await service.getRepresentantes(req);
        res.json({ representantes });
    }
}


