import { Request, Response } from "express";
import service from "../services/getTests/service";

export class TestController {
    public async getTests(req: Request, res: Response): Promise<void> {
        const tests = await service.getTests(req);
        res.json({ tests });
    }
}


