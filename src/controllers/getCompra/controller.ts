import { Request, Response } from "express";
import serviceGet from "../../services/getCompra/service";
import servicePost from "../../services/postCompra/service";

export class GetComprasController {
    public async getCompras(req: Request, res: Response): Promise<void> {
        const compras = await serviceGet.getCompra(req);
        res.json({ compras });
    }
    public async postCompras(req: Request, res: Response): Promise<void> {
        const comprasPost = await servicePost.postCompra(req);
        if (comprasPost) {
            res.status(200).send({ "status": "Replace Offer ok. Pending activation" });
        } else {
            res.status(404).send({ "status": "Item Not Found" });
        }
    }
}