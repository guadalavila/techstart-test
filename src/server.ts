import express from "express";
import mongoose from "mongoose";

import compression from "compression";
import cors from "cors";

import { MONGODB_URI } from "./util/secrets";

import { UserRoutes } from "./routes/userRoutes";
import { ApiRoutes } from "./routes/apiRoutes";
import { InvoiceRoutes } from "./routes/invoiceRoutes";
const swaggerUi = require("swagger-ui-express");
import swaggerJSDocs from "swagger-jsdoc";
import * as swaggerDocument from "./openapi-spec/POC_RODAR.json";
import { User } from "./models/user";
import { CustomerRoutes } from "./routes/customerRoutes";
import { ProductoRoutes } from "./routes/productoRoutes";
import { DistributorRoutes } from "./routes/distributorRoutes";
import { ManufacturerRoutes } from "./routes/manufacturerRoutes";
import { TestRoutes } from "./routes/testRoutes";

class Server {
  public app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.routes();
    this.mongo();
  }

  public routes(): void {
    this.app.use("/api/user", new UserRoutes().router);
    this.app.use("/apis", new ApiRoutes().router);
    this.app.use("/api/invoice", new InvoiceRoutes().router);
    this.app.use("/api/customer", new CustomerRoutes().router);
    this.app.use("/api/product", new ProductoRoutes().router);
    this.app.use("/api/distributor", new DistributorRoutes().router);
    this.app.use("/api/manufacturer", new ManufacturerRoutes().router);
    this.app.use("/api/tests", new TestRoutes().router);

    this.app.use(
      "/api-docs",
      swaggerUi.serve,
      swaggerUi.setup(swaggerDocument)
    );
  }

  public config(): void {
    this.app.set("port", process.env.PORT || 3000);
    this.app.use(express.json());
    this.app.use(express.urlencoded({ extended: false }));
    this.app.use(compression());
    this.app.use(cors());
  }

  private mongo() {
    const connection = mongoose.connection;
    connection.on("connected", () => {
      console.log("Mongo Connection Established");
    });
    connection.on("reconnected", () => {
      console.log("Mongo Connection Reestablished");
    });
    connection.on("disconnected", () => {
      console.log("Mongo Connection Disconnected");
      console.log("Trying to reconnect to Mongo ...");
      console.log(MONGODB_URI);
      setTimeout(() => {
        mongoose.connect(MONGODB_URI, {
          autoReconnect: true,
          keepAlive: true,
          socketTimeoutMS: 3000,
          connectTimeoutMS: 3000
        });
      }, 3000);
    });
    connection.on("close", () => {
      console.log("Mongo Connection Closed");
    });
    connection.on("error", (error: Error) => {
      console.log("Mongo Connection ERROR: " + error);
    });

    const run = async () => {
      await mongoose.connect(MONGODB_URI, {
        autoReconnect: true,
        keepAlive: true
      });
    };
    run().catch((error) => console.error(error));
  }

  public start(): void {
    this.app.listen(this.app.get("port"), () => {
      console.log(
        "  API is running at http://localhost:%d",
        this.app.get("port")
      );
    });
  }
}

const server = new Server();

server.start();
