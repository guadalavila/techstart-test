const cuentaCorrienteMock = {
    customer_id: 987654,
    "finantial_accounts": [
        {
            "id_fa": "FA_55678910",
            "business_line": "wireline",
            "status": "actived",
            "balance": 123.45
        },
        {
            "id_fa": "FA_550987654",
            "business_line": "wireless",
            "status": "actived",
            "balance": 12.50
        }
    ]
};

export default cuentaCorrienteMock;