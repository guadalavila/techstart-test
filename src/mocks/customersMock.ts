const customersMock = [
  {
    id: 987654,
    name: "Jose Luis Calderon",
    doc_type: "dni",
    doc_number: "21212121",
    customer_type: "individuos",
    customer_score: 950,
  },
  {
    id: 123456,
    name: "Willy Rosa Huanca",
    doc_type: "dni",
    doc_number: "23232323",
    customer_type: "empleado",
    customer_score: 380,
  },
];

export default customersMock;
