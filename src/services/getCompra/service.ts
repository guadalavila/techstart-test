import { logger } from "../../configuration/log4js";
import { Request } from "express";
import compraMock from "../../mocks/compraMock";

const SOSAmountsUrl = "urlSOS";


const getCompra = async (req: Request) => {
    try {

        logger.info(`\nMETHOD: GET\nDATA: ${JSON.stringify(compraMock)}`);

        return compraMock;
    } catch (e) {
        return {
            data: {
                URL: SOSAmountsUrl,
                METHOD: "GET",
                ERROR: `${e.message} - ${e.response.data.message}`,
            },
            status: e.response.status,
        };
    }
};

export default { getCompra };