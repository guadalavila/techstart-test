import { logger } from "../../configuration/log4js";
import { Request } from "express";
import subscribersMock from "../../mocks/subscribersMock";

const SOSAmountsUrl = "URLMOCKSOS";

function getSubscriberInfo(customer_id: number) {
    if (customer_id === subscribersMock.customer_id) {
        return subscribersMock;
    }
}

const getSubscribers = async (req: Request) => {
    try {
        const data = getSubscriberInfo(parseInt(req.params.customer_id));

        logger.info(`\nMETHOD: GET\nDATA: ${JSON.stringify(data)}`);

        return data;
    } catch (e) {
        logger.error(`\nURL: ${SOSAmountsUrl}\nMETHOD: GET\nERROR: ${e.message} - ${e.response.data.message}`);
        return {
            data: {
                URL: SOSAmountsUrl,
                METHOD: "GET",
                ERROR: `${e.message} - ${e.response.data.message}`,
            },
            status: e.response.status,
        };
    }
};

export default { getSubscribers };