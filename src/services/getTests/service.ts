import { logger } from "../../configuration/log4js";
import { Request } from "express";
import testsMock from "../../mocks/testsMock";

const apiUri = "APIURI";

function filterRepresentantes(id: number) {
    return testsMock;
}

const getTests = async (req: Request) => {
    try {
        const data = filterRepresentantes(parseInt(req.params.id));
        logger.info(`\nMETHOD: GET\nDATA: ${JSON.stringify(data)}`);
        return data;
    } catch (e) {
        logger.error(
            `\nURL: ${apiUri}\nMETHOD: GET\nERROR: ${e.message} - ${e.response ? e.response.data.message : e}`
        );
        return {
            data: {
                URL: apiUri,
                METHOD: "GET",
                ERROR: `${e.message} - ${e.response ? e.response.data.message : undefined}`,
            },
            status: e.response ? e.response.status : undefined,
        };
    }
};

export default { getTests };
