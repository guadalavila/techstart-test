import { Document, Schema, Model, model, Error } from "mongoose";

export interface IProducto extends Document {
  Id: String;
  Description: String;
  ProductCode: String;
  DistributorLocationId: String;
  ManufacturerId: String;
}

export const productoSchema = new Schema({
  Id: {
    type: String,
    required: true,
    unique: true
  },
  Description: String,
  ProductCode: String,
  DistributorLocationId: {
    ref: "Distributor",
    type: Schema.Types.ObjectId
  },
  ManufacturerId: {
    ref: "Manufacturer",
    type: Schema.Types.ObjectId
  }
});

export const Producto: Model<IProducto> = model<IProducto>(
  "Producto",
  productoSchema
);
